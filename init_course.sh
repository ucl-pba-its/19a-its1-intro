#!/usr/bin/env bash 

source project-info

FILES="README.md docs/${REPOSLUG_US}_lecture_plan.md  .gitlab-ci.yml exercises/exercises_prepend.md weekly_plans/weekly_prepend.md site/_config.yml"

OLDREPO="course-template"
OLDSLUG="COURSE-TEMPLATE"
OLDSLUG_US="COURSE_TEMPLATE"
OLDTITLE="COURSE TEMPLATE"

echo Replacing: 
echo "$OLDREPO -> $REPONAME"
echo "$OLDSLUG -> $REPOSLUG"
echo "$OLDSLUG_US -> $REPOSLUG_US"
echo "$OLDTITLE -> $TITLE"

git mv docs/COURSE_TEMPLATE_lecture_plan.md docs/${REPOSLUG_US}_lecture_plan.md

sed -i	-e "s/$OLDREPO/$REPONAME/" \
	-e "s/$OLDSLUG/$REPOSLUG/" \
	-e "s/$OLDSLUG_US/$REPOSLUG_US/" \
	-e "s/$OLDTITLE/$TITLE/" \
	$FILES

