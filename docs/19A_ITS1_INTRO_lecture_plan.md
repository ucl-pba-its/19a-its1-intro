---
title: '19A PBa IT sikkerhed'
subtitle: 'Fagplan for Introduktion til IT sikkerhed'
main_author: 'Morten Bo Nielsen/MON'
email: 'mon@ucl.dk'
left-header: \today
right-header: 'Fagplan for Introduktion til IT sikkerhed'
skip-toc: true
semester: 19A
filename: 19A_ITS1_INTRO_lecture_plan
---

# Introduktion

Formålet med faget er at få alle studerende op på et fælles grundniveau.

De studerende vil have forskellige baggrund og dermed kompentencer når det starter. Dette vil vi bruge til at få de studerende til at hjælpe hinanden og på den blie skarpe på deres egne styrker og svagheder.


Faget er på 5 ECTS point.

# Læringsmål

Viden
Den studerende har viden om og forståelse for:

* Grundlæggende programmeringsprincipper
* Grundlæggende netværksprotokoller
* Sikkerhedsniveau i de mest anvendte netværksprotokoller

Færdigheder
Den studerende kan supportere løsning af sikkerhedsarbejde ved at:

* Anvende primitive datatyper og abstrakte datatyper
* Konstruere simple programmer der bruge SQL databaser
* Konstruere simple programmer der kan bruge netværk
* Konstruere og anvende tools til f.eks. at opsnappe samt filtrere netværkstrafik
* Opsætte et simpelt netværk.
* Mestre forskellige netværksanalyse tools
* Læse andres scripts samt gennemskue og ændre i dem

Kompetencer
Den studerende kan:

* Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv



Se [Studieordningen sektion 2.1](https://www.ucl.dk/link/3a5f054541da4bbca44c677abf1ee8c3.aspx?nocache=77112065-b434-4e74-a5b8-336327053716)

# Lektionsplan

| Underviser | Uge | Indhold |
| :---: | :---: | :--- |
| MON | 36 | Introduktion til faget. Forventningsafstemning, Kompentence afklaring, valg af præsentationsemne |
| MON | 37 | Git, gitlab og projektstyring |
| MON | 38 | Programmering i python og C# | både socket og sqlite/mysql |
| MON | 39 | Scripting i bash og powershell | e.g config update eller gitlab CI |
| MON | 40 | Networking basics | ISO model, IP, ports, packages, wireshark, mirror ports |
| MON | 41 | Kali linux fun | network tools, sniffing scanning and such |
| MON | 42 | Efterårsferie | skemafri, ikke ferie |
| MON | 43 | Eksamen | portfolio eksamen baseret på tidligere ugeres arbejde. |
