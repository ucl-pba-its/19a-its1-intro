---
title: '19A ITS1 Introduktion til IT sikkerhed'
subtitle: 'Eksamensaflevering'
main_author: 'Morten Bo Nielsen'
date: 2019-10-27
email: 'mon@ucl.dk'
left-header: 2019–10–27
right-header: Eksamensaflevering
skip-toc: true
filename: 19a-its1-intro-eksamensaflevering
---

# Introduktion

I [fagplanen](https://ucl-pba-its.gitlab.io/19-its-docs/19A-ITS-introduktion-til-it-sikkerhed.pdf) er læringsmålene beskrevet sammen med listen over emner per uge.

Basalt set, så var det ideen et de studerende skulle introduceres for nøgleteknologier, og give klassen en fælles baseline.

Dette er et fag med en intern eksamen, dvs. ingen censor. Eksamensformen er en aflevering på wiseflow, som bliver bedømt efter 7-trins skalaen.

# Rapporten indhold

Formålet med rapporten er at dokumentere opfyldelse af læringsmålene, og at dokumentere at der er arbejdet med alle fagets ugeemner.

Målgruppen for rapporten er underviseren, som må forventes at have et højt niveau indenfor de respektive ugeemner, dvs. detaljerede beskrivelser af almindelig teknologi er unødvendig - muligvist er det kontraproduktivt.

Faget har indeholdt 5 ugeemner. Disse er listen nedenfor med forslag til rapport indhold.

* Git, gitlab og projektstyring

    Beskriv git og gitlab, og hvordan vi bruger det (og kan bruge det) i vores kontekst

* Programmering i python og C#

    Vis eksempler på at begge programmerginssprog er blevet brugt.

    Dette kunne inkludere links til gitlab repositories. Denne slags vil formelt blive set som appendikser.

* Scripting i bash og powershell


    Vis eksempler på at begge scripting sprog er blevet brugt.

    Dette kunne inkludere links til gitlab repositories. Denne slags vil formelt blive set som appendikser.

* Networking basics

    Vis eksempler på brug af netværk. Dette vil inkludere diagrammer med interfaces, IP adresser, mv.

    Eksemplet med load balancer viser IP adresser, flere subnets, flere interfaces, tests, mm. At man formidler en god forståelse for det system ville vise god forståelse for de fleste vigtige netværkselementer

* Kali linux fun

    Kali er et vigtigt værktøj, og afleveringen skal vise en forståelse for hvad Kali er og kan bruges til. Inkluder eksempler på hvad det bliver brugt til.


# Formalia

Rapporten skal opfylde de almindelige formalia, som der er for rapporter. Der tillades en rimelig bred forståelse af dette.

* Navn, klasse, fag og dato skal være på forsiden.
* Husk sidenumre
* Rapporten skal være pæn og læselig
* Husk at forklare hvad der ses i billeder/screenshots og hvorfor det er relevant.
* Omfanget er ca. 10 sider, ekskl. forside, indholdsfortegnelse, appendikser, mv.
