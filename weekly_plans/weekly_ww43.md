---
Week: 43
Content:  Recap
Material: Se links i ugeplanen
Initials: MON
---

\pagebreak

# Uge 43

## Mål for ugen

Praktiske og læringsmål for ugen

### Praktiske mål
Ingen

### Lærings mål
* Den studerende har overblik over emnerne i intro forløbet
* Den studerende ved hvad eksamensafleveringen skal indeholde

## Leverancer
Ingen

## Tidsplan

Nedenfor er den foreløbige plan for dagen. Den vil blive opdateret med input fra studerende, m.fl.

### Monday

| Time | Activity |
| :---: | :--- |
| 8:15  | Eksamensafleveringen |
|       | Recap af aktiviteter, checkup af materiale fra ugerne |
|       | Niveau snak |
| 11:30 | Lunch break |
| 12:15 | TBD |

## Hands-on opgaver

Guided workshop style øvelser. Detaljer kommer på dagen.



## Kommentarer
* Studerende står for dele af undervisningen - broken link: https://gitlab.com/ucl-its-19/19a-intro/blob/master/Emner_uge_stud.jpg
