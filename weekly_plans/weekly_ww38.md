---
Week: 38
Content:  Programmering i python og C#
Material: Se links i ugeplanen
Initials: MON
---

\pagebreak

# Uge 38

## Mål for ugen

Praktiske og læringsmål for ugen

### Praktiske mål
* Alle har adgang til et udviklingsmiljø til både python og C#
* Alle ar lavet simple programmer både i C# og i python

### Lærings mål
* Den studerende kan skrive simple programmer i C#
* Den studerende kan skrive simple programmer i python

## Leverancer
Ingen

## Tidsplan

Nedenfor er den foreløbige plan for dagen. Den vil blive opdateret med input fra studerende, m.fl.

### Monday

| Time | Activity |
| :---: | :--- |
| 8:15  | Vi starter |
| 11:30 | Lunch break |
| 12:15 | Vi forstætter :-) |

Link:

* Python [crawler exercise repo](https://gitlab.com/OrganicOrangeJuice/crawler)
* C# [ecercise repo](https://gitlab.com/matias0103/workshop/tree/master/WorkshopProgram)

## Hands-on opgaver

Der er forberedelse til dagen (jvf. besked på riot)

* installere [pycharm](https://www.jetbrains.com/pycharm/) for python

  vi kan bruge vores skole @-mail til at få PyCharm pro gratis + andre af JetBrains programmer i vores studie tid, til dem der har lyst til det. [Se hjemmesiden](https://www.jetbrains.com/student/)

* installere [visual studio](https://visualstudio.microsoft.com/vs/community/) til C# delen (på windows)

  Der skal også installeres .NET Desktop Development. Dette kan gøres ved at åbne visual studio installer -> klik på modify -> tilføj .NET desktop development. Det skulle gerne kun fylde omkring 3 GB


Guided workshop style øvelser. Detaljer kommer på dagen.



## Kommentarer
* Studerende står for dele af undervisningen - broken link: https://gitlab.com/ucl-its-19/19a-intro/blob/master/Emner_uge_stud.jpg
