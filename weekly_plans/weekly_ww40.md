---
Week: 40
Content:  Networking
Material: Se links i ugeplanen
Initials: MON
---

\pagebreak


# Uge 40

## Mål for ugen

Praktiske og læringsmål for ugen

### Praktiske mål

TBA

### Lærings mål

TBA

## Leverancer
Ingen

## Tidsplan

Nedenfor er den foreløbige plan for dagen. Den vil blive opdateret med input fra studerende, m.fl.

### Monday

| Time | Activity |
| :---: | :--- |
| 8:15  | wireshark øvelser |
| 11:30 | Lunch break |
| 12:15 | fortsat |

Links:

* [pcap øvelser](http://www.malware-traffic-analysis.net/training-exercises.html)

## Hands-on opgaver

Guided workshop style øvelser. Detaljer kommer på dagen.



## Kommentarer
* Studerende står for dele af undervisningen - broken link: https://gitlab.com/ucl-its-19/19a-intro/blob/master/Emner_uge_stud.jpg
