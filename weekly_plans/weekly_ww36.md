---
Week: 36
Content:  Intro til faget
Material: Se links i ugeplanen
Initials: MON
---

# Week 36

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Den studerende ved hvor man kan finde dokumenter og den slags
* Klassen kender hinandens faglige kompetencer
* Det er besluttet hvem der præsenterer hvilket emne
* Alle har hul igennem til gitlab

### Learning goals
None at this time.

## Deliverables
* Kompetence selv-evaluering

## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Monday

| Time  | Activity |
| :---: | :--- |
| 12:15 | MON introducerer uddannelse og faget. Forventningsafstemning |
| 13:00 | Kompetence selveval + pause |
| 13:30 | Diskussion og emnevalg |


## Hands-on time

Exercise er beskrevet i [exercise dokumentet](https://ucl-pba-its.gitlab.io/19a-its1-intro/19A_ITS1_intro_exercises.pdf)

## Comments
* Er der nogen som er interesseret i Red Teaming?
* Marketing vil gerne have en af Jer til at Instagramme på torsdag.
* Studerende står for dele af undervisningen - broken link: https://gitlab.com/ucl-its-19/19a-intro/blob/master/Emner_uge_stud.jpg
