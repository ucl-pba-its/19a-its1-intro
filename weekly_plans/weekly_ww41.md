---
Week: 41
Content:  Kali fun
Material: Se links i ugeplanen
Initials: MON
---

\pagebreak

# Uge 41

## Mål for ugen

Praktiske og læringsmål for ugen

### Praktiske mål
Ingen

### Lærings mål
* Den studerende har kendskab til Kalis værktøjskasse

## Leverancer
Ingen

## Tidsplan

Nedenfor er den foreløbige plan for dagen. Den vil blive opdateret med input fra studerende, m.fl.

### Monday

| Time | Activity |
| :---: | :--- |
| 8:15  | proxychains, macchanger |
| 11:30 | Lunch break |
| 12:15 | metasploit |


Links:

* Metasploit:

    Det er 4 episoder af en serie der giver en intro til modulerne i metasploit, og fremgangsmåden for at bruge værktøjet.

    * [Metasploit For Beginners - #1](https://www.youtube.com/watch?v=8lR27r8Y_ik)
    * [Metasploit For Beginners - #2](https://www.youtube.com/watch?v=6SNHuWaLuhU)
    * [Metasploit For Beginners - #3](https://www.youtube.com/watch?v=CYB6Uta9VTI)
    * [Metasploit For Beginners - #4](https://www.youtube.com/watch?v=AyMgYhwyGSE)


## Hands-on opgaver

Guided workshop style øvelser. Detaljer kommer på dagen.




## Kommentarer
* Studerende står for dele af undervisningen - broken link: https://gitlab.com/ucl-its-19/19a-intro/blob/master/Emner_uge_stud.jpg
