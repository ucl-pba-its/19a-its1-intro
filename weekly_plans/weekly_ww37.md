---
Week: 37
Content:  Git, gitlab og projektstyring
Material: Se links i ugeplanen
Initials: MON
---

\pagebreak

# Uge 37

## Mål for ugen

Praktiske og læringsmål for ugen

### Praktiske mål
* Alle har ssh adgang til gitlab.com og kan push'e kode fra kommando linien.

### Lærings mål
* Den studerende kan bruge git til simpel versionsstyring
* Den studerende kan bruge gitlab til simpel CI
* Den studerende kender til gitlab som projekt- og kommunikationsværktøj

## Leverancer
Ingen

## Tidsplan

Nedenfor er den foreløbige plan for ugen. Den vil blive opdateret med input fra studerende, m.fl.

### Mandag

| Time | Activity |
| :---: | :--- |
| 8:15  | Intro til git |
| 9:00  | Git workshop |
| 11:00 | Intro til gitlab
| 11:30 | Lunch break |
| 12:15 | Intro til gitlab issues, board og milestones |
| 13:00 | Gitlab CI og pages |
| ? | Ekstra opgaver hvis vi er hutigt færdige |

Links:

* En introduktion til Git og installationsvejledning til div. OS’er på [git-scm.com](https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F)
* [Git reference](https://git-scm.com/docs)



## Hands-on opgaver

Guided workshop style øvelser. Detaljer kommer på dagen.



## Kommentarer
* Studerende står for dele af undervisningen - broken link: https://gitlab.com/ucl-its-19/19a-intro/blob/master/Emner_uge_stud.jpg
* Jeg har tilføjet frokostpause til tidsplanen - så er der bedre chance for at vi husker det.
* For git workshop, use [this link](https://eal-itt.gitlab.io/git-demo/about/)
