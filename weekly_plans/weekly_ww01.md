---
Week: 01
Content:  Project statup
Material: Se links i ugeplanen
Initials: MON
---

\pagebreak

# Uge XX

## Mål for ugen

Praktiske og læringsmål for ugen

### Praktiske mål
* ..
* ..

### Lærings mål
* ..
* ..

## Leverancer
* ..
* ..


## Tidsplan

Nedenfor er den foreløbige plan for ugen. Den vil blive opdateret med input fra studerende, m.fl.

### Monday

| Time | Activity |
| :---: | :--- |
| 8:15  | ... |
| 11:30 | Lunch break |
| 12:15 | Presentations and discussions |



## Hands-on opgaver

### Opgave 1

![this is an example image](ucl_logo_raw.png)

...



## Kommentarer
* ..
* ..
