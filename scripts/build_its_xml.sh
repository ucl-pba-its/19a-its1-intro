#!/usr/bin/env bash

set -e

source project-info

LECTURE_MD=$(ls docs | grep -i lecture | grep md)

if [ "x$LECTURE_MD" == "x" ]; then
	echo "no lecture plan found"
	exit 0
fi

cd scripts
echo "Using lecture plan $LECTURE_MD"

# enable this if testing locally
#virtualenv -p python3 venv
#source venv/bin/activate

pip3 install -r requirements.txt

python3 gen_itslearning_xml.py -i ../docs/$LECTURE_MD -p "https://ucl-pba-its.gitlab.io/$REPONAME" > ../site/assets/itslearning.xml
